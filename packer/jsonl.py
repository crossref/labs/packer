import json
import os
import tarfile


def process_item(item, output_file):
    """Process a single item"""
    output_file.write(json.dumps(item))
    output_file.write("\n")


def make_tarfile(output_filename, source):
    with tarfile.open(output_filename, "w:gz") as tar:
        tar.add(source, arcname=os.path.basename(source))
