import datetime
import gzip
import logging
import tarfile
from pathlib import Path

import orjson as json
from rich.progress import track


def iterate_all(data_directory) -> (dict, str, int):
    """Iterate over all DOIs in the data directory"""
    data_path = Path(data_directory)
    gz_files = list(data_path.glob("*.gz"))

    # determine if this is single file or distributed
    plus = len(gz_files) == 1

    if not plus:
        logging.info("Loading public data dump")
        for file in track(gz_files):
            with gzip.open(file, "rb") as f_handle:
                json_contents = json.loads(f_handle.read())
                location = 0

                for json_item in json_contents["items"]:
                    yield json_item, file, location

                    location = location + 1
    else:
        logging.info("Loading Plus data dump")
        for file in gz_files:
            with tarfile.open(file, "r:gz") as tarf:
                for member in track(tarf):
                    with tarf.extractfile(member) as f_handle:
                        json_contents = json.loads(f_handle.read())

                        location = 0

                        for json_item in json_contents["items"]:
                            yield json_item, file, location

                            location = location + 1


def date_parts_to_date(input_list: list):
    """Convert date parts to a date"""
    if input_list and input_list[0][0] is not None:
        try:
            year = input_list[0][0]
        except IndexError:
            year = 1970

        try:
            month = input_list[0][1]
        except IndexError:
            month = 1

        try:
            day = input_list[0][2]
        except IndexError:
            day = 1

        return datetime.date(year, month, day)
    else:
        return None
