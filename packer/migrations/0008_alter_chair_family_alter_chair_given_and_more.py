# Generated by Django 5.0 on 2023-12-18 14:37

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("packer", "0007_item_relation"),
    ]

    operations = [
        migrations.AlterField(
            model_name="chair",
            name="family",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name="chair",
            name="given",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name="chair",
            name="sequence",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name="editor",
            name="family",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name="editor",
            name="given",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name="editor",
            name="sequence",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name="translator",
            name="family",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name="translator",
            name="given",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name="translator",
            name="sequence",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
