# Generated by Django 5.0 on 2023-12-18 14:09

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("packer", "0003_alter_author_authenticated_orcid"),
    ]

    operations = [
        migrations.AddField(
            model_name="item",
            name="event",
            field=models.ManyToManyField(blank=True, null=True, to="packer.event"),
        ),
    ]
