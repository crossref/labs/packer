# Crossref Data Dump Repacker
A python application that allows you to repack the Crossref data dump into different formats.

![license](https://img.shields.io/gitlab/license/crossref/labs/packer) ![activity](https://img.shields.io/gitlab/last-commit/crossref/labs/packer)

![Git](https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white) ![GitHub](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white) ![Linux](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black) ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)

This command-line application allows you to repack the annual data dump from Crossref into different formats.

## Featureset

* JSON-L packer: complete

## Usage
    Usage: packer.py [OPTIONS] COMMAND [ARGS]...
    
      Build a corpus of academic articles and score them for jargon
    
    Options:
      --help  Show this message and exit.
    
    Commands:
      compress              Compresses the document into a tar.gz file
      pack-to-single-jsonl  Packs all DOIs into a JSON-L document
      upload-to-s3          Uploads a file to s3


# Credits
* [Git](https://git-scm.com/) from Linus Torvalds _et al_.
* [.gitignore](https://github.com/github/gitignore) from Github.
* [Rich](https://github.com/Textualize/rich) from Textualize.

&copy; Crossref 2023