import inspect
import os.path
import sqlite3
import sys

import click
from claws.aws_utils import AWSConnector

from packer.jsonl import make_tarfile


sys.path.append(
    os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "packer.settings")

import django

django.setup()
from packer import api, jsonl


@click.group()
def cli():
    """
    Build a corpus of academic articles and score them for jargon
    """
    pass


@click.command()
@click.argument("output")
@click.option(
    "--data-directory",
    default="/home/martin/sixteenTB/April2022",
    help="The directory containing an annual Crossref data dump",
)
def pack_to_single_jsonl(output, data_directory):
    """
    Packs all DOIs into a JSON-L document
    """
    with open(output, "w") as output_file:
        for item, file_name, location in api.iterate_all(data_directory):
            jsonl.process_item(item, output_file)

    make_tarfile(output + ".tar.gz", output)


@click.command()
@click.argument("output")
def compress(output):
    """
    Compresses the document into a tar.gz file
    """
    make_tarfile(output + ".tar.gz", output)


@click.command()
@click.argument("in_file")
def upload_to_s3(in_file):
    """
    Uploads a file to s3
    """
    from smart_open import open

    aws_connector = AWSConnector(
        unsigned=False,
        bucket="org.crossref.research.retractionwatch",
    )

    bucket = "outputs.research.crossref.org"

    url = f"s3://{bucket}/jsonl/single.jsonl.tar.gz"

    with open(in_file, "rb") as fin:
        with open(
            url,
            "wb",
            transport_params={"client": aws_connector.s3_client},
        ) as fout:
            data = fin.read(2048)

            while data:
                fout.write(data)
                fout.flush()
                data = fin.read(2048)


if __name__ == "__main__":
    cli.add_command(compress)
    cli.add_command(pack_to_single_jsonl)
    cli.add_command(upload_to_s3)

    cli()
